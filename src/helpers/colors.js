export const COLORS = {
    white : "#ffffff",
    dark  : "#051023",
    dark1 : "#0c1728",
    dark2 : "#020c1f",
    dark3 : "#263349",
    dark4 : "#141d2d",
    green : "#00ff62",
    green2: "#68FF00",
    green3: "#12ffb9",
    green4: "#00ff1c",
    blue  : "#2668fc",
    gray  : "#586782",
    
    greenGradient: "linear-gradient(to right, #00ff62, #68FF00)",
}