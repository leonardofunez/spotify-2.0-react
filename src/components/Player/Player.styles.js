import styled from "styled-components"
import { COLORS } from "../../helpers/colors"
import { NavLink } from "react-router-dom"
import IconsImage from "../../assets/img/icons/icons.svg"
import { Slider, SliderBar } from "../Globals/Globals.styles"

// Breakpoints
import { breakpoint } from "../../helpers/breakpoint"

export const Container = styled.section`
    grid-column: content-start / span 2;
    grid-row: player-start;
    display: grid;
    grid-template-rows: 
        [player-top] 25px 
        [player-bottom] 50px;
    grid-template-columns: 1fr;
    
    background-color: ${COLORS.green};
    background-image: ${COLORS.greenGradient};
    box-shadow: inset 0 7px 18px -7px rgba(0, 0, 0, .8);
    color: ${COLORS.dark};
    padding: 10px 10px 0 10px;
    position: relative;

    ${breakpoint.sm} {
        grid-template-rows: 1fr;
        grid-template-columns: 
            [player-left] 1fr 
            [player-center] 300px 
            [player-right] 1fr;
        padding: 0 30px;
    }

    ${breakpoint.md} {
        grid-column: menu-start / -1;
    }
`;

export const ProgressBar = styled.div`
    position: absolute;
    height: 2px;
    width: 50%;
    top: 0;
    left: 0;
    background: ${COLORS.green3};

    ${({widthBar}) => widthBar && `width: ${widthBar}%;`}
`;

export const Track = styled.div`
    display: flex;
    align-items: center;
    text-align: center;
    grid-row: player-top;
    justify-content: center;

    ${breakpoint.sm} {
        grid-row: 1;
        justify-content: flex-start;
        text-align: left;
        min-width: 0;
    }
`;

export const Photo = styled.div`
    height: 30px;
    min-width: 30px;
    border-radius: 10%;
    margin-right: 10px;
    display: none;

    ${({src}) => src && `background: url(${src}) no-repeat center / cover ${COLORS.white};`}

    ${breakpoint.sm} {
        display: block;
        border-radius: 30%;
        height: 55px;
        min-width: 55px;
    }
`;

export const Info = styled.div`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const SongName = styled.p`
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const ArtistName = styled(NavLink)`
    font-size: 12px;
    font-weight: 500;
    color: ${COLORS.dark};
    opacity: 0.6;

    &:hover {
        text-decoration: underline;
    }

    ${breakpoint.sm} {
        opacity: 1;
    }
`;

export const Controls = styled.div`
    display: flex;
    justify-self: center;
    align-self: center;

    grid-row: player-bottom;

    ${breakpoint.sm} {
        grid-row: 1;
    }
`;

export const Control = styled.button`
    height: 40px;
    width: 40px;
    margin: 0 10px;
    border-radius: 6px;
    position: relative;

    ${({noPadding}) => noPadding ? `padding: 0;` : `padding: 10px;`}

    &:before {
        content: "";
        display: block;

        ${({posX}) => posX && `background-position-x: ${posX}px;`}
        ${({posY}) => posY && `background-position-y: ${posY}px;`}
        ${({size}) => size && `background-size: ${size}px;`}

        background-image: url(${IconsImage});
        background-repeat: no-repeat;

        ${({noPadding}) => noPadding ? `
            height: 40px;
            width: 40px;
        ` : `
            height: 20px;
            width: 20px;
        `}
    }

    &:after {
        height: 4px;
        width: 4px;
        position: absolute;
        border-radius: 50%;
        background-color: ${COLORS.dark};
    }
    
    ${({isShuffle}) => isShuffle && `
        &:after {
            content: "";
            top: 14px;
            right: 7px;
        }
    `}

    ${({isRepeat}) => isRepeat && `
        &:after {
            content: "";
            top: 14px;
            right: 7px;
        }
    `}

    &:hover {
        background-color: rgba(${COLORS.white}, $alpha: 0.3);
    }
`;

export const Volume = styled.div`
    width: 100%;
    max-width: 175px;
    position: relative;

    grid-row: player-top;
    grid-column: player-right;
    display: none;

    ${breakpoint.sm} {
        display: flex;
        align-items: center;
        justify-self: end;
        grid-column: player-right;
        grid-row: 1;
    }
`;

export const VolumeButton = styled.div`
    background: url(${IconsImage}) no-repeat -292px -184px / 560px;
    ${({isMuted}) => isMuted && `background-position: -171px -184px;`}
    ${({isLow})   => isLow   && `background-position: -211px -184px;`}
    ${({isMid})   => isMid   && `background-position: -252px -184px;`}
    ${({isHigh})  => isHigh  && `background-position: -292px -184px;`}
    
    height: 30px;
    width: 32px;
    cursor: pointer;
    margin-right: 5px;
`;

export const VolumeSlider = styled(Slider)`
    width: 142px;
`;

export const VolumeSliderBar = styled(SliderBar)``;