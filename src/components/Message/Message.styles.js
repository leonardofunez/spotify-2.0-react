import styled from "styled-components"
import { COLORS } from "../../helpers/colors"

export const Container = styled.div`
    font-size: 14px;

    i {
        color: ${COLORS.green};
    }
`;